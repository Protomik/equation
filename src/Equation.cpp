#include "Equation/Equation.h"
#include <iostream>
#include <math.h>


	Quadratic::Quadratic(double a, double b, double c) {
		this->a = a;
		this->b = b;
		this->c = c;
	};

	void Quadratic::setVariables() {
		std::cout << "Enter A,B and C" << std::endl;
		std::cin >> a >> b >> c;

		while (a == 0) {
			if (a == 0) {
				std::cout << "A =/= 0" << std::endl;
				std::cin >> a;
			}
		}

		std::cout << "A = " << a << std::endl;
		std::cout << "B = " << b << std::endl;
		std::cout << "C = " << c << std::endl;
	};

	void Quadratic::setA() {
		std::cout << "Enter A" << std::endl;
		std::cin >> a;

		while (a == 0) {
			if (a == 0) {
				std::cout << "A =/= 0" << std::endl;
				std::cin >> a;
			}
		}
	};

	void Quadratic::setB() {
		std::cout << "Enter B" << std::endl;
		std::cin >> b;
	};

	void Quadratic::setC() {
		std::cout << "Enter C" << std::endl;
		std::cin >> c;
	};


	void Quadratic::calculateEquation() {
		std::cout << " D = " << b*b - 4 * a*c << std::endl;
		if ((-1 * b + sqrt(b*b - 4 * a*c)) / 2 * a == (-1 * b - sqrt(b*b - 4 * a*c)) / 2 * a)
			std::cout << " X = " << (-1 * b + sqrt(b*b - 4 * a*c)) / (2 * a) << std::endl;
		else {
			std::cout << " X1 = " << (-1 * b + sqrt(b*b - 4 * a*c)) / (2 * a) << std::endl;
			std::cout << " X2 = " << (-1 * b - sqrt(b*b - 4 * a*c)) / (2 * a) << std::endl;
		};
	};
