#ifndef  EQUATION_H
#define  EQUATION_H

class Quadratic {

public:
	Quadratic (double a, double b, double c);
	void setVariables();
	void setA();
	void setB();
	void setC();
	void calculateEquation();


private:
	double a;
	double b;
	double c;
};

#endif