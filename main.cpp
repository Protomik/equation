#include "Equation/Equation.h"
#include <stdio.h>

int main (int argc, char ** argv) {
	Quadratic Quadratic(1, 2, 0);
	Quadratic.calculateEquation();

	printf("\n");

	Quadratic.setVariables();
	Quadratic.calculateEquation();

	printf("\n");

	Quadratic.setA();
	Quadratic.setB();
	Quadratic.setC();
	Quadratic.calculateEquation();

	return 0;
}


